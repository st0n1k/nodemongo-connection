const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const uri = 'mongodb+srv://admin:admin@cluster0-7sgao.mongodb.net/Confusion?retryWrites=true&w=majority';
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect(err => {
    assert.equal(err, null);
    console.log('Connected corectly to server');
  const collection = client.db("Confusion").collection("dishes");
  // perform actions on the collection object
  collection.insertOne({"name": "Uthappizza", "description": "test"}, (err, result) => {
      assert.equal(err, null);

      console.log('After Insert:\n');
      console.log(result.ops);

      collection.find({}).toArray((err, docs) => {
        assert.equal(err, null);
        console.log('Found:\n');
        console.log(docs);

        client.close();
      });
  });
});